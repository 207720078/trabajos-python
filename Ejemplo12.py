#EP12. Cree un diagrama de flujo que dado un año A indique si es o no
#bisiesto. Un año es bisiesto si es múltiplo de 4, exceptuando los
#múltiplos de 100, que sólo son bisiestos cuando son múltiplos de 400,
#por ejemplo el año 1900 no fue bisiesto, pero el año 2000 si lo fue.

year = int(input("Ingrese el año deseado: "))

if (year % 4 == 0 and year % 100 != 0 or year % 400 == 0):
    print("El año", year, "es bisiesto")
else:
    print("El año", year, "no es bisiesto")


