#EP13. Para ingresar a un curso de alto nivel, los candidatos deben
#realizar tres exámenes, A, B, C. Diseñe un algoritmo que reciba como
#entrada la edad y las notas de los tres exámenes. Calcule y despliegue
#el promedio de notas obtenido y un mensaje indicando si fue aceptado o
#no en el curso sabiendo que:
#–Tener entre 15 y 18 años inclusive y obtener promedio de los exámenes mayor a 90.
#-Tener más de 18 años y que su promedio esté entre 80 y 90 inclusive.
#–Tener menos de 15 años y un promedio mayor o igual a 90, o un promedio
#mayor o igual a 80 pero no debe obtener menos de un 85 en la nota del
#examen c.

edad = int(input("Ingrese la edad: "))
nota1 = int(input("Ingrese la primera nota: "))
nota2 = int(input("Ingrese la segunda nota: "))
nota3 = int(input("Ingrese la tercera nota: "))

promedio = (nota1 + nota2 + nota3)

if edad >= 15 and edad <= 18 and promedio > 90:
    print("Ha sido aprobado al curso")
elif edad > 18 and promedio >= 80 and promedio <= 90:
    print("Ha sido aprobado al curso")
elif edad < 15 and promedio >= 80 and nota3 >= 85:
    print("Ha sido aprobado al curso")
else:
    print("Reprobado")


