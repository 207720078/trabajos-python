#EP22. Un coleccionista y vendedor de carros antiguos desea imprimir a un
#cliente la lista de los carros que tiene su empresa y que cumplan con la
#condición que dicho cliente pide. Autos con número de placa entre 5000
#y 8000, y que sean modelo 1958 o 1959. Diseñe un algoritmo que reciba
#como entrada el número de placa y el año de un auto e imprima la si el
#vehículo podría o no interesarle al cliente.

NumeroPlaca = int(input("Ingrese el numero de placa: "))
modelo = int(input("Ingrese el modelo: "))

if ((NumeroPlaca >= 5000) and (NumeroPlaca < 8001)) and ((modelo == 1958) or (modelo == 1959)):
    print("Le interesa al cliente")
else:
    print("No le interesa al cliente")


