#EP19. Diseñe un algoritmo que reciba el largo y el ancho de un terreno y
#calcule el área; si dicha área es mayor a 100 m2, entonces el sistema
#deberá imprimir como salida “Terreno apto para construcción”, caso
#contrario se imprimirá “Terreno no apto para construcción”.

largo = int(input("Ingrese el largo: "))
ancho = int(input("Ingrese el ancho: "))

area = (largo*ancho)

if area >= 100:
    print("Terreno apto para construir")
else:
    print("Terreno no apto para construir")


