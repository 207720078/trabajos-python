#EP14. Diseñe un algoritmo que reciba como entrada las horas trabajadas
#y el salario por hora que recibe un trabajador, calcule el salario
#total y neto sabiendo que la deducción del Seguro Social es del 9%.

HorasTrabajadas = int(input("Ingrese las horas trabajadas: "))
SalarioHora = int(input("Ingrese el salario por hora: "))

SalarioTotal = (HorasTrabajadas*SalarioHora)*0.9

print("El salario total que recibe un trabajador con la reduccion del Seguro Social es: ", SalarioTotal)


