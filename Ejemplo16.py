#EP16. Diseñe un algoritmo que reciba un número entero, si dicho número es
#positivo, entonces se debe imprimir la raíz cuadrada del mismo, si el
#número es negativo se debe imprimir el cuadrado y el cubo.

import math

entero = int(input("Ingrese el numero entero: "))


if entero > 0:
    raiz = math.sqrt(entero)
    print("La raiz cuadrada de",entero,"es: ",raiz)
else:
    cuadrado = pow(entero, 2)
    cubo = pow(entero, 3)
    print("El cuadrado de", entero, "es: " , cuadrado, "y el cubo es: ", cubo)


#
#




