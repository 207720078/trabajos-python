#EP21. Diseñe un algoritmo que reciba como entradas las horas trabajadas y
#el salario por hora de un trabajador y calcule su salario neto tomando
#en cuenta que: si el salario bruto es menor o igual a ¢100 000 se le
#rebaja un 10% de impuesto, pero si el salario bruto es mayor a ¢100 000 se
#le rebajará un 10% sobre los primeros 100 000 y un 15% sobre el exceso de
#los ¢100 000. El sistema debe imprimir el salario neto
#(salario bruto – monto del impuesto).

HorasTrabajadas = int(input("Ingrese las horas trabajadas: "))
SalarioHora = int(input("Ingrese el salario por hora: "))

bruto = (HorasTrabajadas*SalarioHora)
R = (bruto*0.10)
N = (bruto-R)

if bruto <= 1000:
    print("El salario neto es: ", N)
else:
    valor = 100000
    bruto2 = (bruto-valor)
    I2 = (bruto2*0.15)
    N2 = (bruto-I2-valor)
    print("El salario neto es: ", N2)




