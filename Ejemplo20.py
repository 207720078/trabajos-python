#EP20. Diseñe un algoritmo que reciba como entradas el peso de un cargamento
#y calcule el monto a pagar por su transporte, tomando en cuenta que: si el
#peso total es menor a 50 toneladas, cada una cuesta 1000 colones, si es
#mayor o igual a 50 toneladas, cada una de ellas cuesta 1500.

peso = int(input("Ingrese el peso: "))

if peso >= 50:
    A = (peso*1500)
    print("El total a pagar es: ", A)
else:
    B = (peso*1000)
    print("El total a pagar es: ", B)


