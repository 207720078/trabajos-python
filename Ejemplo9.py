#EP9. Realice un algoritmo que determine el pago a realizar por la entrada
#a un espectáculo donde se pueden comprar sólo hasta cuatro entrada, donde
#al costo de dos entradas se les descuenta el 10%, al de tres entrada el 15%
#y a la compra de cuatro tickets se le descuenta el 20 %.

entradas=int(input("Cuantas entradas : "))
ticket = 1000

if entradas > 4:
    print("No se permite mas de 4 entradas ")
elif entradas == 2:
    descuento = (entradas*ticket)*0.1
    totalPagar = (entradas*ticket)-descuento
    print("El costo total de dos entradas es: ", totalPagar)
elif entradas == 3:
    descuento = (entradas*ticket)*0.15
    totalPagar = (entradas*ticket)-descuento
    print("El costo total de tres entradas es: ", totalPagar)
elif entradas == 4:
    descuento = (entradas*ticket)*0.2
    totalPagar = (entradas*ticket)-descuento
    print("El costo total de cuatro entradas es: ", totalPagar)
elif entradas == 1:
    print("El valor de su compra es: ", ticket)

