#EP23. La empresa “Compre Aquí” desea estimular a sus empleados, haciéndoles
#un aumento de salario, que va de acuerdo a la categoría del empleado de
#la siguiente manera:
#–Si la categoría es 1 el aumento es del 15% sobre el salario actual.
#–Si la categoría es 2 el aumento será de un 20% sobre el salario actual.
#–Si la categoría es 3, el aumento será de un 25% sobre el salario actual.
#Diseñe un algoritmo que reciba como entrada la categoría y el salario
#actual del trabajador,
#calcule e imprima el nuevo salario.

categoria = int(input("Ingrese la categoria: "))
SalarioActual = int(input("Ingrese el salario actual: "))

if categoria == 1:
    salarioNuevo = SalarioActual + (SalarioActual*0.15)
    print("EL salario nuevo es: ", salarioNuevo)
elif categoria == 2:
    salarioNuevo = SalarioActual + (SalarioActual*0.20)
    print("El salaio nuevo es: ", salarioNuevo)
elif categoria == 3:
    salarioNuevo = SalarioActual + (SalarioActual*0.25)
    print("El salario nuevo es: ", salarioNuevo)




