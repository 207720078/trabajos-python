

MensajeEntrada = "programa de generacion de tablas de multiplicar".capitalize()
print(MensajeEntrada.center(60, "="))


#opcion = 1
opcion = 'S'
#Ejemplo para salir con numeros
#while opcion!=0:
#Ejemplo para salir con letras
while opcion!='n' or opcion!='N': #while principal
    #Se ingresan los valores de las tablas
    TabInicio = int(input(chr(27) + "[1;35m" + "Ingrese la tabla inicial: "))
    TabFinal = int(input(chr(27) + "[1;34m" + "Ingrese la tabla final: "))
#Recordatorio de seguimiento de código en las tablas(error)
    while TabFinal < TabInicio:
        print("La tabla de inicio debe ser menor")
        TabInicio = int(input("Ingrese la tabla inicial: "))
        TabFinal = int(input("Ingrese la tabla final: "))

#Se ingresa el rango de las tablas
    RangoInicio = int(input("Ingrese el rango inicial: "))
    RangoFinal = int(input("Ingrese el rango final: "))
#Recordatorio de seguimiento de codigo en los rangos(error)
    while RangoFinal < RangoInicio:
        print("El rango inicial debe ser menor")
        RangoInicio = int(input(chr(27) + "[1;31m" + "Ingrese el rango inicial: "))
        RangoFinal = int(input(chr(27) + "[1;92m" + "Ingrese el rango final: "))

#Se pone el +1 para indicarle a Python que no empiece desde 0
    while TabInicio < TabFinal:
        for i in range(TabInicio,TabFinal+1):

            if i == 4: #Si el indice es igual a 4, al correr suprimirá la tabla del 4(independientemente si es 4 o cualquier numero)
                print("La tabla del {0} no la imprimo porque no quiero" .format(i))
                continue #Se brinca la tabla del 4 y continua con el ciclo
            for j in range(RangoInicio,RangoFinal+1):
                if j == 5: #Si el indice es igual a 5, al correr tirará todos los resultados exceptuando al 5(independientemente si es 5 o no)
                    print(chr(27) + "[1;31m" + "Más de 5 no quiero: ") #Cambiar color del codigo
                    break #Suprime el resultado de la multiplicacion del 5 y termina el ciclo

                resultado = i*j
                #print("Multiplicar", i, " * ", j," es igual a: ", resultado) #Forma de imprimir el codigo al correrlo
                print("Multiplicar %d * %d es igual a: %d "%(i, j, resultado)) #Forma de imprimir el codigo al correrlo

        TabInicio = TabInicio+1
#Se elimina el int ya que se estan ingresando letras y no numeros enteros
    opcion = input("Desea ejecutar nuevamente el proceso: \n" #enter
                       "s: Continuar \n"
                       "n: Salir \n")
    if (opcion == 'n') or (opcion == 'N'):
        break
        #Si se termina el ciclo con un break, el print final no va a salir puesto que está destinado a un fin correcto de ciclo
else:
    print("Gracias por jugar")




